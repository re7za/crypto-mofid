/** @type {import('next').NextConfig} */

const nextConfig = {
  // reactStrictMode: true,
  swcMinify: true,
  images: {
    path: '/_next/image/',
    domains: ['assets.coingecko.com', '127.0.0.1', 'localhost'],
  },
}

module.exports = nextConfig
