import {
  createElement,
  ElementType,
  FunctionComponent,
  HTMLAttributes,
} from 'react'
import { twMerge } from 'tailwind-merge'
import clsx from 'clsx'

interface IButtonProps extends HTMLAttributes<Element> {
  component?: ElementType
  iconBtn?: boolean
  disabled?: boolean
}

const Button: FunctionComponent<IButtonProps> = (props) => {
  const { component = 'button', className, iconBtn, ...otherProps } = props
  const { disabled } = otherProps

  const defaultStyle = clsx(
    `
    bg-green-600 dark:bg-grey-100 px-6 py-2
    cursor-pointer rounded hover:bg-green-700
    dark:hover:bg-grey-300 text-white
    dark:text-green-1000 transition font-medium
  `,
    disabled && 'bg-grey-600 dark:bg-grey-600'
  )
  const iconBtnStyle = iconBtn ? `rounded-full px-2 py-2` : ''

  return createElement(component, {
    className: twMerge(defaultStyle, iconBtnStyle, className),
    ...otherProps,
  })
}

export default Button
