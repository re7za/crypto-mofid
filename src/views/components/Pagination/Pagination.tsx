import {
  Fragment,
  FunctionComponent,
  HTMLAttributes,
  ReactNode,
  useMemo,
  useRef,
  useState,
} from 'react'
import { twMerge } from 'tailwind-merge'
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/24/outline'
import { scrollToRef } from '../../../utils/window'

interface IPaginationProps extends HTMLAttributes<Element> {
  children?: ReactNode
  onPageChange: (val: number) => void
  totalPages: number
  currentPage?: number
  className?: string
}

interface IPageProps {
  className?: string
  value?: number
  children: ReactNode
  doNothing?: boolean
}

const range = (start: number, to: number) => {
  return [...Array(to - start + 1 || 0)].map((_, i) => i + start)
}

const fetchPaginationData = (
  totalPages: number,
  selectedPage: number,
  maximumNeighbours = 1
) => {
  const minBound = Math.max(1, selectedPage - maximumNeighbours)
  const maxBound = Math.min(totalPages, selectedPage + maximumNeighbours)
  return {
    isEnd: selectedPage === totalPages,
    hasLastPage: totalPages - selectedPage > 1,
    has3EndingDots: totalPages - selectedPage > 2,
    pages: range(minBound, maxBound),
    hasFirstPage: selectedPage > 2,
    has3StartingDots: selectedPage > 3,
    isStart: selectedPage === 1,
  }
}

const Pagination: FunctionComponent<IPaginationProps> = (props) => {
  const {
    children,
    onPageChange,
    totalPages,
    currentPage = 1,
    className,
  } = props
  const [selectedPage, setSelectedPage] = useState(currentPage)

  const {
    isEnd,
    hasLastPage,
    has3EndingDots,
    isStart,
    hasFirstPage,
    has3StartingDots,
    pages,
  } = useMemo(
    () => fetchPaginationData(totalPages, selectedPage),
    [totalPages, selectedPage]
  )

  const firstElementRef = useRef(null)

  const scrollTop = () => {
    scrollToRef(firstElementRef)
  }

  const handlePageChange = (newPage: number) => {
    scrollTop()
    setSelectedPage(newPage)
    onPageChange?.call(this, newPage)
  }

  const Page = ({ className, value, children, doNothing }: IPageProps) => (
    <li
      key={value}
      className={twMerge(
        'flex items-center justify-center mx-1 h-8 w-8',
        className
      )}
      {...(doNothing ? {} : { onClick: () => handlePageChange(value || 1) })}
    >
      {children}
    </li>
  )

  const liClass = 'cursor-pointer h-8 w-8 select-none dark:text-grey-50 '
  const currentLiClass = `bg-green-600 dark:bg-grey-100 text-white dark:text-green-1000 rounded`
  const restDotsClass = 'w-8 h-8 select-none'

  return (
    <div ref={firstElementRef}>
      {children}
      {!(totalPages < 2) && (
        <ul
          className={twMerge(
            'flex justify-center m-0 p-2 list-none',
            className
          )}
        >
          <>
            {!isStart && (
              <Page className={liClass} value={selectedPage - 1}>
                <ChevronLeftIcon width={24} />
              </Page>
            )}
            {hasFirstPage && (
              <Page
                className={twMerge(liClass, isStart ? currentLiClass : '')}
                value={1}
              >
                1
              </Page>
            )}
            {selectedPage === 4 ? (
              <Page className={liClass} value={2}>
                2
              </Page>
            ) : (
              has3StartingDots && (
                <Page doNothing className={restDotsClass}>
                  ...
                </Page>
              )
            )}
            {pages.map((pageNumber) => (
              <Fragment key={'pagination-' + pageNumber}>
                <Page
                  className={twMerge(
                    liClass,
                    selectedPage === pageNumber ? currentLiClass : ''
                  )}
                  value={pageNumber}
                >
                  {pageNumber}
                </Page>
                {!(totalPages < 6) && (
                  <>
                    {isStart && pageNumber === 2 && (
                      <Page key="const-3" className={liClass} value={3}>
                        3
                      </Page>
                    )}
                    {((isStart && pageNumber === 2) ||
                      (selectedPage === 2 && pageNumber === 3)) && (
                      <Page key="const-4" className={liClass} value={4}>
                        4
                      </Page>
                    )}
                  </>
                )}
              </Fragment>
            ))}
            {selectedPage === totalPages - 3 ? (
              <Page
                className={twMerge(
                  liClass,
                  selectedPage === totalPages - 1 ? currentLiClass : ''
                )}
                value={totalPages - 1}
              >
                {totalPages - 1}
              </Page>
            ) : (
              has3EndingDots && (
                <Page doNothing className={restDotsClass}>
                  ...
                </Page>
              )
            )}
            {hasLastPage && (
              <Page
                className={twMerge(
                  liClass,
                  selectedPage === totalPages ? currentLiClass : ''
                )}
                value={totalPages}
              >
                {totalPages}
              </Page>
            )}
            {!isEnd && (
              <Page className={liClass} value={selectedPage + 1}>
                <ChevronRightIcon width={24} />
              </Page>
            )}
          </>
        </ul>
      )}
    </div>
  )
}

export default Pagination
