import { FunctionComponent, HTMLAttributes } from 'react'
import Text from '../Text'
import clsx from 'clsx'
import Button from '../Button'

export type RadioOptionsType = {
  value: any
  label: string
  disabled?: boolean
}

export interface IRadioOptionsProps
  extends HTMLAttributes<Element>,
    RadioOptionsType {
  index: number
  checkedIndex: () => number
  onChange?: (value: any) => void
  checkbox?: boolean
  name: string
  radioDisabled?: boolean
  radioValue: any
  button?: boolean
}

export const RadioOptions: FunctionComponent<IRadioOptionsProps> = (props) => {
  const {
    label,
    value: optionValue,
    disabled: optionDisabled,
    checkedIndex,
    index,
    onChange,
    checkbox,
    radioDisabled,
    radioValue,
    button,
    ...otherProps
  } = props
  const { name } = otherProps

  const id = `${name}_${optionValue}`
  const isChecked = index === checkedIndex()

  const handleInputChange = () => {
    if (!onChange) return
    if (checkbox) {
      return onChange(optionValue === radioValue ? '' : optionValue)
    }
    return onChange(optionValue)
  }

  const inputRadio = (
    <input
      id={id}
      type={checkbox ? 'checkbox' : 'radio'}
      checked={isChecked}
      onChange={handleInputChange}
      className={clsx(
        button &&
          `appearance-none w-full h-full
            absolute inset-0`
      )}
      {...otherProps}
      disabled={optionDisabled}
    />
  )

  return button ? (
    <Button
      className={clsx(
        'relative py-1 px-4',
        isChecked && 'bg-green-900 dark:bg-grey-400'
      )}
      key={id}
    >
      {inputRadio}
      {label}
    </Button>
  ) : (
    <div
      className={clsx(
        `flex flex-row tems-start`,
        radioDisabled ? 'cursor-default' : 'cursor-pointer'
      )}
      key={id}
    >
      {inputRadio}
      <Text
        component="label"
        className={clsx(
          'pl-2',
          optionDisabled ? 'cursor-default' : 'cursor-pointer'
        )}
        {...(!optionDisabled ? { htmlFor: id } : {})}
      >
        {label}
      </Text>
    </div>
  )
}
