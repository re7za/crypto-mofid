import { FunctionComponent, InputHTMLAttributes } from 'react'
import {
  IRadioOptionsProps,
  RadioOptions,
  RadioOptionsType,
} from './RadioOptions'

export interface IRadioProps
  extends Omit<
      InputHTMLAttributes<Element>,
      'checked' | 'onChange' | 'value' | 'name'
    >,
    Omit<
      IRadioOptionsProps,
      'index' | 'checkedIndex' | 'label' | 'radioValue' | 'radioDisabled'
    > {
  options: RadioOptionsType[]
}

const Radio: FunctionComponent<IRadioProps> = (props) => {
  const { options, value, ...otherProps } = props
  const { disabled: radioDisabled } = otherProps

  const checkedIndex = () => options.findIndex((el) => el.value === value)

  return (
    <>
      {options.map((option, i) => (
        <RadioOptions
          key={option.value}
          index={i}
          checkedIndex={checkedIndex}
          radioDisabled={radioDisabled}
          radioValue={value}
          {...option}
          {...otherProps}
        />
      ))}
    </>
  )
}

export default Radio
