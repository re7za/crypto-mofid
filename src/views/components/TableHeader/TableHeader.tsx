import { FunctionComponent } from 'react'
import Text from '../Text'
import clsx from 'clsx'
import { twMerge } from 'tailwind-merge'

export type ColumnType = {
  title: string
  className?: string
}

interface ITableHeaderProps {
  className?: string
  columns: ColumnType[]
}

const TableHeader: FunctionComponent<ITableHeaderProps> = (props) => {
  const { columns, className } = props
  return (
    <div
      className={twMerge(
        `flex w-full px-8 py-2 sticky top-0 
        bg-white dark:bg-green-1000 z-10 `,
        className
      )}
    >
      {columns?.map((column) => (
        <Text
          key={column.title}
          className={clsx(
            'text-sm text-grey-500 dark:text-grey-400',
            column.className
          )}
        >
          {column.title}
        </Text>
      ))}
    </div>
  )
}

export default TableHeader
