export const getColorClass = (num: number) => {
  if (num > 0) return 'text-green-600 dark:text-green-500'
  if (num < 0) return 'text-red-600 dark:text-red-500'
  return ''
}
