import { FunctionComponent } from 'react'
import { stringManipulator } from '../../../utils/string'
import Text from '../Text'
import { TokenType } from './types'
import { getColorClass } from './utils'
import clsx from 'clsx'
import Image from 'next/image'

interface ITokensItemProps extends TokenType {}

const TokenItem: FunctionComponent<ITokensItemProps> = (props) => {
  const {
    name,
    symbol,
    image,
    current_price,
    price_change_percentage_24h,
    price_change_percentage_7d_in_currency,
    market_cap,
    total_supply,
    circulating_supply,
  } = props

  return (
    <div className="flex items-center py-3 px-8 border-b border-grey-300 dark:border-grey-800">
      <div className="w-1/6 flex items-center">
        {image ? (
          <div className="mr-4 flex items-center">
            <Image src={image} alt={symbol} width={30} height={30} />
          </div>
        ) : (
          <></>
        )}
        <div>
          <Text className="font-semibold">{name}</Text>
          <Text
            className={clsx(
              'leading-4 text-sm text-grey-500 dark:text-grey-400'
            )}
          >
            {symbol?.toLocaleUpperCase()}
          </Text>
        </div>
      </div>
      <Text className="w-1/12 text-right font-semibold">
        {stringManipulator(current_price, {
          commaSeparated: true,
          dashIfEmpty: true,
          currencySign: '$',
          toFixed: 2,
        })}
      </Text>
      <Text
        className={clsx(
          'w-1/12 text-right',
          getColorClass(price_change_percentage_24h)
        )}
      >
        {stringManipulator(price_change_percentage_24h, {
          dashIfEmpty: true,
          isPercentage: true,
          toFixed: 2,
          plusIfPositive: true,
        })}
      </Text>
      <Text
        className={clsx(
          'w-1/12 text-right',
          getColorClass(price_change_percentage_7d_in_currency)
        )}
      >
        {stringManipulator(price_change_percentage_7d_in_currency, {
          dashIfEmpty: true,
          isPercentage: true,
          toFixed: 2,
          plusIfPositive: true,
        })}
      </Text>
      <Text className="w-1/6 text-right">
        {stringManipulator(market_cap, {
          commaSeparated: true,
          dashIfEmpty: true,
          currencySign: '$',
          toFixed: 0,
        })}
      </Text>
      <Text className="w-1/6 text-right">
        {stringManipulator(total_supply, {
          commaSeparated: true,
          dashIfEmpty: true,
          currencySign: '$',
          toFixed: 0,
        })}
      </Text>
      <Text className="w-1/4 text-right">
        {stringManipulator(circulating_supply, {
          dashIfEmpty: true,
        })}
        {circulating_supply ? (
          <span className="ml-2 text-grey-500 dark:text-grey-400">
            {symbol}
          </span>
        ) : (
          <></>
        )}
      </Text>
    </div>
  )
}

export default TokenItem
