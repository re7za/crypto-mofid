import Head from 'next/head'
import path from 'path'
import { FunctionComponent } from 'react'
import { pagesMetaData, pageNamesEnum } from '../../../constants/pagesMetaData'
import { setting } from '../../../constants/setting'

export interface IPageMetaTagsProps {
  pageName: pageNamesEnum
}

const PageMetaTags: FunctionComponent<IPageMetaTagsProps> = (props) => {
  const { pageName } = props
  const constantMetaData = pagesMetaData[pageName] || {}

  const pageTitle: string = constantMetaData.meta_title
  const pageDescription: string = constantMetaData.meta_title

  return (
    <Head>
      <meta charSet="utf-8" />
      <title>{pageTitle}</title>
      <meta name="description" content={pageDescription} />
      <link
        rel="icon"
        href={path.posix.join(setting.staticDir, `brand/mofid-logo.jpg`)}
      />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"
      />
    </Head>
  )
}

export default PageMetaTags
