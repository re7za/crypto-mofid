import { FunctionComponent, useState } from 'react'
import Container from '../../layouts/Container'
import PairsList from '../../modules/PairsList'
import TokensTable from '../../modules/TokensTable'

interface IHomePageTemplateProps {}

const HomePageTemplate: FunctionComponent<IHomePageTemplateProps> = (props) => {
  const [pair, setPair] = useState<string>('usd')

  const handlePairChange = (pair: string) => {
    setPair(pair)
  }

  return (
    <>
      <PairsList pair={pair} onChange={handlePairChange} />
      <Container>
        <TokensTable pair={pair} />
      </Container>
    </>
  )
}

export default HomePageTemplate
