import { ColumnType } from '../../components/TableHeader'

export const columns: ColumnType[] = [
  {
    title: 'COIN',
    className: 'w-1/6',
  },
  {
    title: 'PRICE',
    className: 'w-1/12 text-right',
  },
  {
    title: '24H',
    className: 'w-1/12 text-right',
  },
  {
    title: '7D',
    className: 'w-1/12 text-right',
  },
  {
    title: 'MARKET CAP',
    className: 'w-1/6 text-right',
  },
  {
    title: 'TOTAL VOLUME',
    className: 'w-1/6 text-right',
  },
  {
    title: 'CIRCULATING SUPPLY',
    className: 'w-1/4 text-right',
  },
]
