import { FunctionComponent, useState } from 'react'
import { useGetTokensList } from '../../../hooks/services/useGetTokensList'
import Pagination from '../../components/Pagination'
import TableHeader from '../../components/TableHeader'
import TokenItem, { TokenType } from '../../components/TokenItem'
import { columns } from './constants'

interface ITokensTableProps {
  pair: string
}

const hypotheticalTotalPages = 100

const TokensTable: FunctionComponent<ITokensTableProps> = (props) => {
  const { pair } = props
  const [page, setPage] = useState<number>(1)

  const refetchInterval: number = 60 * 1000 // 1 minutes
  const { data, isLoading, isError } = useGetTokensList(
    {
      page,
      pair,
    },
    {
      refetchInterval,
      keepPreviousData: true,
    }
  )

  const handlePageChange = (page: number) => {
    setPage(page)
  }

  if (isLoading) return <div>fancy loading..</div>
  if (isError) return <div>handle error some how!</div>

  return (
    <div className="relative">
      <TableHeader columns={columns} className={'mt-8'} />
      <Pagination
        onPageChange={handlePageChange}
        totalPages={hypotheticalTotalPages}
        currentPage={page}
        className="my-8"
      >
        <div>
          {data?.data?.map((token: TokenType) => (
            <TokenItem key={token.id} {...token} />
          ))}
        </div>
      </Pagination>
    </div>
  )
}

export default TokensTable
