import { FunctionComponent, useContext, useMemo } from 'react'
import { PairsContext } from '../../../constants/PairsContext'
import Radio, { RadioOptionsType } from '../../components/Radio'

interface IPairsListProps {
  pair: string
  onChange: (pair: string) => void
}

const PairsList: FunctionComponent<IPairsListProps> = (props) => {
  const { pair, onChange } = props
  const pairs = useContext(PairsContext)

  const pairsRadioOptions: RadioOptionsType[] = useMemo(
    () =>
      pairs?.map((pair) => ({
        label: pair,
        value: pair,
      })),
    pairs
  )

  return (
    <div className="flex w-full overflow-x-scroll bg-grey-100 dark:bg-green-1000 gap-x-3 py-3 px-5">
      <Radio
        name="pairs"
        options={pairsRadioOptions}
        onChange={onChange}
        value={pair}
        button
      />
    </div>
  )
}

export default PairsList
