import type { NextPage, GetServerSideProps } from 'next'
import { dehydrate, QueryClient } from 'react-query'
import {
  tokensListKey,
  getDataFromFetchTokensList,
} from '../hooks/services/useGetTokensList'
import HomePageTemplate from '../views/templates/HomePageTemplate'
import GeneralPageWrapper from '../wrappers/GeneralPageWrapper'
import { pageNamesEnum } from '../constants/pagesMetaData'
import { tokensPairsList } from '../constants/urls'
import { request } from '../utils/axios-utils'
import { PairsContext } from '../constants/PairsContext'

interface IHomePageProps {
  pairs: string[]
}

const Home: NextPage<IHomePageProps> = (props) => {
  const { pairs } = props

  return (
    <GeneralPageWrapper pageName={pageNamesEnum.HOME}>
      <PairsContext.Provider value={pairs}>
        <HomePageTemplate />
      </PairsContext.Provider>
    </GeneralPageWrapper>
  )
}

export default Home

export const getServerSideProps: GetServerSideProps = async () => {
  const pairs = await request(tokensPairsList)

  const queryClient = new QueryClient()
  await queryClient.prefetchQuery(tokensListKey, getDataFromFetchTokensList)

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
      pairs: pairs?.data,
    },
  }
}
