import '../styles/globals.css'
import type { AppProps } from 'next/app'
import ReactQueryWrapper from '../wrappers/ReactQueryWrapper'
import ThemeWrapper from '../wrappers/ThemeWrapper'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeWrapper>
      <ReactQueryWrapper {...pageProps}>
        <Component {...pageProps} />
      </ReactQueryWrapper>
    </ThemeWrapper>
  )
}

export default MyApp
