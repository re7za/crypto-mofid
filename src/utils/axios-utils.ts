import axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios'
import { setting } from '../constants/setting'

const onRequest = (config: AxiosRequestConfig): AxiosRequestConfig => {
  if (config.headers) {
    config.headers['content-type'] = 'application/json'

    // if we had authorization system we could add the following line:
    // config.headers.common['Authorization'] = `Bearer token`
  }
  return config
}
const onRequestError = (error: AxiosError): Promise<AxiosError> => {
  return Promise.reject(error)
}

const onResponse = (response: AxiosResponse): AxiosResponse => {
  // We can handle some general operations here
  return response
}
const onResponseError = (error: AxiosError): Promise<AxiosError> => {
  // We can handle server side messages here, for example pass them to an alert if we had
  return Promise.reject(error)
}

export function setupInterceptorsTo(
  axiosInstance: AxiosInstance
): AxiosInstance {
  axiosInstance.interceptors.request.use(onRequest, onRequestError)
  axiosInstance.interceptors.response.use(onResponse, onResponseError)
  return axiosInstance
}

const axiosInstance: AxiosInstance = axios.create({
  baseURL: setting.baseUrl,
})
export const request: AxiosInstance = setupInterceptorsTo(axiosInstance)
