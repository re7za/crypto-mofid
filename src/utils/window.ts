import { MutableRefObject } from 'react'

export const scrollToRef = (ref: MutableRefObject<any>) => {
  if (typeof window === 'undefined') return
  window.scrollTo(0, ref?.current?.offsetTop)
}
