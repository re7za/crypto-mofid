import { AnyObjectType } from '../types/commonTypes'

export const object2queryParams = (obj: AnyObjectType): string => {
  return (
    '?' +
    Object.keys(obj)
      .filter((k) => obj[k] !== null)
      .map((k) => `${k}=${obj[k]}`)
      .join('&')
  )
}
