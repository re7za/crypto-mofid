import { commaSeparator, fixNumberTo } from './number'

type stringManipulatorArgType = {
  currencySign?: '$'
  currencySymbol?: string
  isPercentage?: boolean
  toFixed?: number
  dashIfEmpty?: boolean
  commaSeparated?: boolean
  plusIfPositive?: boolean
}

export const stringManipulator = (
  str: string | number,
  options: stringManipulatorArgType
): string => {
  const {
    currencySign,
    currencySymbol,
    isPercentage,
    toFixed,
    dashIfEmpty,
    commaSeparated,
    plusIfPositive,
  } = options

  if (dashIfEmpty && !str) return '-'

  let tempString: string = str.toString()
  if (typeof toFixed === 'number') tempString = fixNumberTo(tempString, toFixed)
  if (commaSeparated) tempString = commaSeparator(tempString)
  if (isPercentage) tempString = `${tempString}%`
  if (currencySign) tempString = `${currencySign}${tempString}`
  if (currencySymbol) tempString = `${tempString} ${currencySymbol}`
  if (plusIfPositive && Number(str) > 0) tempString = `+${tempString}`
  return tempString
}
