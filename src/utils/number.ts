export const fixNumberTo = (
  num: string | number | undefined,
  fixTo?: number
): string => {
  if (['string', 'number'].includes(typeof num))
    return Number(num).toFixed(fixTo)

  return ''
}

export const commaSeparator = (num: number | string): string => {
  const numInString = num.toString()
  if (numInString.includes('.')) {
    const [integer, fractional] = numInString.split('.')
    const separatedInteger = commaSeparator(integer)
    return `${separatedInteger}.${fractional}`
  }
  const separatedNumber = Number(num).toLocaleString('en-US')
  return separatedNumber
}
