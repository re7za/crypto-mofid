import { createContext } from 'react'

export const PairsContext = createContext<string[]>([])
