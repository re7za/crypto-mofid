import { AnyObjectType } from '../types/commonTypes'
import { object2queryParams } from '../utils/objects'

export type TokensListParamsType = {
  pair?: string
  page?: number
  per_page?: number
  price_change_percentage?: string
} & AnyObjectType

export const getTokensListUrl = (params: TokensListParamsType): string => {
  const {
    pair = 'usd',
    page = 1,
    per_page = 20,
    price_change_percentage = '24h,7d',
    ...otherParams
  } = params
  const queryParams: string = object2queryParams({
    vs_currency: pair,
    page,
    per_page,
    price_change_percentage,
    ...otherParams,
  })
  return `/api/v3/coins/markets${queryParams}/`
}

export const tokensPairsList = '/api/v3/simple/supported_vs_currencies/'
