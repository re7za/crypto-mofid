type settingType = {
  baseUrl: string
  isDevelopment: boolean
  SSGRevalidateTime: number
  staticDir: string
}

/**
 * default values
 */

export const setting: settingType = {
  baseUrl: 'https://api.coingecko.com',
  isDevelopment: process.env.NODE_ENV === 'development',
  SSGRevalidateTime: 10 * 60, // 10 Minutes
  staticDir: '/',
}
