import { AnyObjectType } from '../types/commonTypes'

export enum pageNamesEnum {
  HOME = 'HOME',
}

type MetaDataType = {
  meta_title: string
  meta_description?: string
  meta_url?: string
} & AnyObjectType

type PageMetaDataType = {
  [key in pageNamesEnum]: MetaDataType
}

export const pagesMetaData: PageMetaDataType = {
  HOME: {
    meta_title: 'لیست ارز های دیجیتال',
    meta_description: 'توضیحاتی درمورد لیست کامل ارز های دیجیتال',
    meta_url: 'https://bimebazar.com/',
  },
}
