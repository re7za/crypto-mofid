import { FunctionComponent, HTMLAttributes, ReactNode } from 'react'
import { MoonIcon, SunIcon } from '@heroicons/react/24/outline'
import { useTheme } from '../../hooks/useTheme'
import Button from '../../views/components/Button'
import clsx from 'clsx'

interface IThemeWrapperProps extends HTMLAttributes<Element> {
  children?: ReactNode
}

const themeIcons = {
  light: () => <MoonIcon width={36} className="text-white" />,
  dark: () => <SunIcon width={36} className="dark:text-black" />,
}

const ThemeWrapper: FunctionComponent<IThemeWrapperProps> = (props) => {
  const { children } = props
  const { theme, toggleTheme } = useTheme()

  return (
    <>
      {children}
      <Button
        iconBtn
        onClick={toggleTheme}
        className={clsx('fixed', 'bottom-5', 'right-5', 'shadow-2xl')}
      >
        {themeIcons[theme]()}
      </Button>
    </>
  )
}

export default ThemeWrapper
