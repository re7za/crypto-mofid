import { FunctionComponent, ReactNode, useRef } from 'react'
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import { setting } from '../../constants/setting'

interface IReactQueryWrapperProps {
  children: ReactNode
  dehydratedState?: any
}

const ReactQueryWrapper: FunctionComponent<IReactQueryWrapperProps> = (
  props
) => {
  const { children, dehydratedState } = props
  const queryClient = useRef(new QueryClient())

  return (
    <QueryClientProvider client={queryClient.current}>
      <Hydrate state={dehydratedState}>{children}</Hydrate>
      {setting.isDevelopment ? (
        <ReactQueryDevtools initialIsOpen={false} />
      ) : (
        <></>
      )}
    </QueryClientProvider>
  )
}

export default ReactQueryWrapper
