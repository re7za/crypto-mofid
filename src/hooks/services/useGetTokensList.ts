import { AxiosResponse } from 'axios'
import { QueryKey, useQuery, UseQueryOptions } from 'react-query'
import { TokensListParamsType, getTokensListUrl } from '../../constants/urls'
import { request } from '../../utils/axios-utils'

export const tokensListKey: QueryKey = 'tokens-list'

export const fetchTokensList = (
  queryParams: TokensListParamsType
): Promise<AxiosResponse> => {
  const url: string = getTokensListUrl(queryParams)
  return request(url)
}

export const getDataFromFetchTokensList = async () => {
  const res = await fetchTokensList({})

  if (res && res.data) {
    return {
      data: res.data,
    }
  } else return {}
}

type UseQueryOptionsType = Omit<
  UseQueryOptions<unknown, unknown, any, string>,
  'queryKey' | 'queryFn'
>

export const useGetTokensList = (
  params: TokensListParamsType,
  options?: UseQueryOptionsType
) => {
  const cacheKeys = Object.keys(params).map((key) => params[key])
  return useQuery(
    [tokensListKey, ...cacheKeys].join(','),
    () => fetchTokensList(params),
    options
  )
}
