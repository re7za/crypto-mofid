# crypto-mofid

The project is a simple crypto currency table.
It is a test application for Mofid security.

## Getting started

After cloning the application you need to run the following commands to install the dependencies:

```
npm install
npm run dev
```

## Build

In order to build the application you need to run the following commands:

```
npm run build
npm run start
```

Thank you
